import ConfigParser, sqlite3 as lite, cgi, bcrypt

from flask import Flask, render_template, request, url_for, g, redirect, flash

app = Flask(__name__)

app.secret_key = 'secretkey'

def connect():
    return lite.connect("database.db")

def userConnect():
    return lite.connect("users.db")

@app.route('/')
def route():
    return redirect('/index/')

@app.route('/index/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
      q = request.form['name']
    return render_template('index.html')

@app.route('/search/', methods=["POST", "GET"])
def search():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection")
    else:
      cur = g.db.execute ("SELECT * FROM collection where (artist like ?) OR (song like ?) OR (album like ?) OR (genre like ?) ORDER BY artist",
      ("%" + check + "%", "%" + check + "%", "%" + check + "%", "%" + check + "%" ))
    search = [dict(artist=row[0], song=row[1], album=row[2], genre=row[3],
    rating=row[4], release=row[5]) for row in cur.fetchall()]
    return render_template('search.html', check=check, search=search)

@app.route('/artists/', methods=["POST", "GET"])
def artists():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT DISTINCT artist FROM collection ORDER BY artist")
    else:
      cur = g.db.execute ("SELECT DISTINCT artist FROM collection where artist like ? ORDER BY artist",
      ("%" + check + "%", ))
    artist = [dict(artist=row[0]) for row in cur.fetchall()]
    return render_template('artist.html', check=check, artist=artist)

@app.route('/songs/')
def song():
    check = request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection ORDER BY artist, song")
    else:
      cur = g.db.execute ("SELECT * FROM collection where song like ? ORDER BY artist, song",
      ("%" + check + "%", ))
    song = [dict(artist=row[0], song=row[1], album=row[2], genre=row[3],
    rating=row[4], release=row[5]) for row in cur.fetchall()]
    return render_template('songs.html', check=check, song=song)

@app.route('/genres/')
def genres():
    check= request.args.get('key', '')
    g.db = connect()
    if check == '':
      cur = g.db.execute ("SELECT * FROM collection ORDER BY genre, artist, song")
    else:
      cur = g.db.execute ("SELECT * FROM collection where genre like ? ORDER BY genre, artist, song",
      ("%" + check + "%", ))
    genre = [dict(artist=row[0], song=row[1], genre=row[3]) for row in cur.fetchall()]
    return render_template('genres.html', check=check, genre=genre)

@app.route('/ratings/')
def ratings():
    g.db = connect()
    cur = g.db.execute ("SELECT * FROM collection ORDER BY rating DESC, artist, song")
    rating = [dict(artist=row[0], song=row[1], rating=row[4]) for row in cur.fetchall()]
    return render_template('ratings.html', rating=rating)

@app.route('/addLogged/')
def logged():
    g.db = userConnect()
    loginCheck = g.db.execute ("SELECT logged from users WHERE logged = 'yes'")
    loginCheck = '\n'.join(''.join(elems) for elems in loginCheck)
    if (loginCheck == 'yes'):
      return redirect(url_for('add'))
    else:
      return redirect(url_for('login'))

@app.route('/accountLogged/')
def accountLogged():
    g.db = userConnect()
    loginCheck = g.db.execute ("SELECT logged from users WHERE logged = 'yes'")
    loginCheck = '\n'.join(''.join(elems) for elems in loginCheck)
    print(loginCheck)
    if (loginCheck == 'yes'):
      return redirect(url_for('account'))
    else:
      return redirect(url_for('login'))

@app.route('/account/')
def account():
    g.db = userConnect()
    filename = ''
    username = g.db.execute ("SELECT username FROM users WHERE logged = 'yes'")
    username = '\n'.join(''.join(elems) for elems in username)
    pic = g.db.execute ("SELECT pic FROM users WHERE logged = 'yes'")
    check = pic
    if (check.fetchone() is not None):
      pic = '\n'.join(''.join(elems) for elems in pic)
      if (pic == 'yes'):
        filename = ('uploads/' + username + '.png')
      #data_uri = open('Aly.png', 'rb').read().encode('base64').replace('\n', '')
      #img_tag = '<img src="data:image/png;base64,{0}">'.format(data_uri)
      #print(img_tag)
      return render_template('account.html', filename=filename, username=username)

@app.route('/accountImage/', methods=['POST', 'GET'])
def accountImage():
    if request.method == 'POST':
      g.db = userConnect()
      username = g.db.execute ("SELECT username FROM users WHERE logged = 'yes'")
      username = '\n'.join(''.join(elems) for elems in username)
      print(username)
      pic = request.files['datafile']
      pic.save('uploads/' + username + '.png')
      upload = g.db.execute ("UPDATE users SET pic = 'yes' WHERE logged = 'yes'")
      g.db.commit()
      return "File Uploaded"
    else:
      page='''
      <html>
      <body>
      <form action="" method="post" name="form" enctype="multipart/form-data">
        <input type="file" name="datafile" />
        <input type="submit" name="submit" id="submit"/>
      </form>
      </body>
      </html>
      '''
      return page, 200

@app.route('/add/', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
      artist = request.form['artist']
      song = request.form['song']
      album = request.form['album']
      genre = request.form['genre']
      rating = request.form['rating']
      release = request.form['release']
      if (artist != '' and song != '' and album != '' and genre != '' and release != ''):
        g.db = connect()
        cur = g.db.execute ("INSERT INTO collection(artist, song, album, genre, rating, release) VALUES (?, ?, ?, ?, ?, ?)", (artist, song, album, genre, rating, release))
        g.db.commit()
      else:
        print "Error, must not leave blank spaces"
    return render_template('add.html')

@app.route('/register/', methods=['GET', 'POST'])
def register():
    message=''
    if request.method == 'POST':
      username = request.form['username']
      password = request.form['password']
      hash = bcrypt.hashpw(password, bcrypt.gensalt())
      if (username != '' and password != ''):
        g.db = userConnect()
        cur = g.db.execute ("SELECT * FROM users WHERE username =?", (username,))
        if cur.fetchone() == None:
          cur = g.db.execute ("INSERT INTO users(username, password, logged, admin) VALUES (?, ?, 'no', 'no')", (username, hash))
          g.db.commit()
          message='Account created, head to the account page to log in!'
        else:
          message='Username taken'
    return render_template('register.html', message=message)

@app.route('/login/', methods=['GET', 'POST'])
def login():
    success=''
    userError=''
    passError=''
    noData=''
    g.db = userConnect()
    if request.method == 'POST':
      username = request.form['username']
      password = request.form['password']
      if (username != '' and password != ''):
        cur = g.db.execute ("SELECT username FROM users WHERE username =?", (username,))
        print(cur.fetchone())
        if cur.fetchone() != '':
          cur2 = g.db.execute ("SELECT password FROM users WHERE username =?",(username,))
          cur2 = '\n'.join(''.join(elems) for elems in cur2)
          print(cur2)
          if cur2 == bcrypt.hashpw(password, cur2):
            print(cur2)
            print(bcrypt.hashpw(password, cur2))
            success = 'You are logged in as ' + username + ' you can now add to the database or head to the account section!'
            print(username)
            cur3 = g.db.execute ("UPDATE users SET logged = 'yes' WHERE username =?",(username,))
            g.db.commit()
          else:
            passError='Password is wrong'
        else:
          userError='Username doesnt exist'
      else:
        noData='Please enter data'
    return render_template('login.html', success=success, userError=userError, passError=passError, noData=noData)

@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    g.db = userConnect()
    cur = g.db.execute ("UPDATE users SET logged = 'no' WHERE logged = 'yes'")
    g.db.commit()
    return redirect(url_for('login'))

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html'), 404

@app.route('/config/')
def config():
  str = []
  str.append('Debug:'+app.config['DEBUG'])
  str.append('port:'+app.config['port'])
  str.append('url:'+app.config['url'])
  str.append('ip_address:'+app.config['ip_address'])
  return '\t'.join(str)

def init(app):
  config = ConfigParser.ConfigParser()
  try:
    config_location = "etc/config.cfg"
    config.read(config_location)

    app.config['DEBUG'] = config.get("config", "debug")
    app.config['ip_address'] = config.get("config", "ip_address")
    app.config['port'] = config.get("config", "port")
    app.config['url'] = config.get("config", "url")
  except:
    print "Could not read configs from: ", config_location

if __name__ == '__main__':
  init(app)
  app.run(
    host=app.config['ip_address'],
    port=int(app.config['port']))
